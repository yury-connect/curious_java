package java_core.freak_action;


public class Run002_Strings {

	public static void main(String[] args) {
		String s1 = new String("111");
		String s2 = "111";
		String s3 = new String("111");
		String s4 = new String(s3);
		
		System.out.println(s1.equals(s2));
		System.out.println(s1.equals(s3));
		System.out.println(s2.equals(s3));
		System.out.println(s4.equals(s3));
	}
}

/*  out:
true
true
true
true
 */

