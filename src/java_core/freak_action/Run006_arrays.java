package java_core.freak_action;


public class Run006_arrays {
	
	/*
	 * присваивание элемента массиву типа int. нужно "закастить"...
	 */
	public static void main(String[] args){
		int[] array = new int[3];
		
		// Присваиваем длинное значение типу int.
//		array[0] = 10l;   // Type mismatch: cannot convert from long to int. Add cast to 'int'
		array[1] = 'd';
		byte b = 10;
		array[2] = b;
	}
	
	public static void main2(String[] args){
		char[][] arr = new char[2][2];
		
		// Присваиваем длинное значение типу int.
//		arr[0][0] = 10l;   // Type mismatch: cannot convert from double to char. Add cast to 'char'
		arr[0][1] = 'a';
		char b = 10;
		arr[1][0] = b;
		
		// Присваивание двойного значения типу символа
//		arr[1][1] = 10.6;   // Type mismatch: cannot convert from double to char. Add cast to 'char'
	}
}

/*  out:
 *  Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
 *  	Type mismatch: cannot convert from long to int
 *  
 *  	at test.Run006_arrays.main(Run006_arrays.java:12)
 */