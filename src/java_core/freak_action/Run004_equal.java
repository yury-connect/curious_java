package java_core.freak_action;


public class Run004_equal {
	
	/*
	 * == и! = Операторы сравнения и 
	 * не равные операторам допускаются с нулем в Java. 
	 * Это может быть полезно при проверке нуля с объектами в Java.
	 */
	
	public static void main(String args[]){

	    // возвращаем истину;
	    System.out.println(null==null);

	    // return false;
	    System.out.println(null!=null);
	    }
}

/*  out:
 *  true
 *  false
 */
