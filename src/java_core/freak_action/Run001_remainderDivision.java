package java_core.freak_action;


public class Run001_remainderDivision {

	public static void main(String[] args) {
		System.out.println(5 % 6);
		System.out.println(6 % 5);
		System.out.println(15 % 6.0);
	}
}

/*  out:
 *  5
 *  1
 *  3.0
 */
