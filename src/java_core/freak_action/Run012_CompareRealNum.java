package java_core.freak_action;

// Сравнение вещественных чисел
public class Run012_CompareRealNum {

    public static void main(String[] args)  {

        // 1.   Прибавляем к нулю 0.1 одиннадцать раз подряд
        double f1 = 0.0;
        for (int i = 1; i <= 11; i++) {
            f1 += .1;
        }

        // 2.   Умножаем 0.1 на 11
        double f2 = 0.1 * 11;

        //должно получиться одно и то же - 1.1 в обоих случаях
        System.out.println("f1 = " + f1);
        System.out.println("f2 = " + f2);

        // Проверим!
        if (f1 == f2) { System.out.println("f1 и f2 равны!"); }
        else{ System.out.println("f1 и f2 не равны!"); }
    }
}

/*
out:
f1 = 1.0999999999999999
f2 = 1.1
f1 и f2 не равны!

Дело в том, что в двоичной системе невозможно точно представить число 0,1 ...
https://javarush.ru/groups/posts/2136-ustroystvo-vejshestvennihkh-chisel
 */
