package java_core.freak_action;


/* Особенности использования Статического класса. 
 * Это вложенный класс, который может обращаться только к статическим полям обертывающего его класса, 
 *  в том числе и приватным. Доступ к нестатическим полям обрамляющего класса 
 *  может быть осуществлен только через ссылку на экземпляр обрамляющего объекта. 
 *  К классу высшего уровня модификатор static неприменим.   */
public class Run010_InnerNested { 
	
	class A { } 
	static class B { }
	
	public static void main(String[] args) { 
//		A a = new A(); /*will fail - compilation error, you need an instance of Run010_InnerNested to instantiate A*/
		B b = new B(); /*will compile successfully, no instance of Run010_InnerNested is needed to instantiate B */
	}
}
