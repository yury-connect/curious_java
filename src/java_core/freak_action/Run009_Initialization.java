package java_core.freak_action;


/* Порядок вызова конструкторов и блоков инициализации двух классов: потомка и его предка:
 * Сначала вызываются все статические блоки от первого предка до последнего наследника. 
 * Потом попарно вызываются динамический блок инициализации и конструктор 
 * в той же последовательности (от предка до последнего потомка).   */
public class Run009_Initialization {
	
	static { System.out.println("Это статический блок инициализации!"); }   // порядок вызова: 1.0
	{ System.out.println("Это нестатический блок инициализации!"); }
//	System.out.println("Эта строка не скомпилируется!");
	
	public static void main(String[] args) {
		Children children = new Children("Rizhick");
//		Interface children2 = new Children("Rizhick");
	}
}

class Parent implements Interface {
	private String name;
	static { System.out.println("Static block in Parent;"); }    // статический (class initializer) порядок вызова: 1.1
	{ System.out.println("First block; class:Parent;"); }   // нестатический (instance initializer) порядок вызова: 2.1
	{ System.out.println("Second block; class:Parent;"); }   // порядок вызова: 2.2
	
	public Parent() { System.out.println("Сonstructor(); class:Parent;"); }
	
	public Parent(String name) { 
		System.out.println("Сonstructor(name); class:Parent; name=" + name + ";");   // порядок вызова: 3
		this.name = name;
	}
}

class Children extends Parent implements Interface {
	private String name;
	static { System.out.println("Static block in Children;"); }   // порядок вызова: 1.2
	{ System.out.println("First block; class:Children;"); }   // порядок вызова: 4.1
	{ System.out.println("Second block; class:Children;"); }   // порядок вызова: 4.2
	
	public Children() { System.out.println("Constructor(); class:Children;"); }
	
	public Children(String name) {
		super(name); // without this will call super(). Если эту строчку убрать, то будет вызван конструктор Parent();
		System.out.println("Сonstructor(name); class:Children; name=" + name + ";");   // порядок вызова: 5
		this.name = name;
	}
}

interface Interface {}

/* out:
 * 
 * Это статический блок инициализации!
 * Static block in Parent;
 * Static block in Children;
 * First block; class:Parent;
 * Second block; class:Parent;
 * Сonstructor(name); class:Parent; name=Rizhick;
 * First block; class:Children;
 * Second block; class:Children;
 * Сonstructor(name); class:Children; name=Rizhick;
 */

// http://javastudy.ru/interview/java-oop2/