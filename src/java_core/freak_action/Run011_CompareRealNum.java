package java_core.freak_action;

// Сравнение вещественных чисел
public class Run011_CompareRealNum {

    public static void main(String[] args)  {

        double f = 0.0;
        for (int i=1; i <= 10; i++) {
            f += 0.1;
        }
        System.out.println(f);
    }
}

/*
Какое число будет выведено на экран? Логичным ответом был бы ответ: число 1.
Мы начинаем отсчет с числа 0.0 и последовательно прибавляем к нему 0.1 десять раз подряд.
Вроде все правильно, должна получиться единица.

out:

0.9999999999999999
 */