package java_core.freak_action;


/*
 * Массивы типов объектов: если мы создаем массивы типов объектов, 
 * то элементы этих массивов могут быть объявленными 
 * объектами типа или объектами дочернего класса.
 */
public class Run007_arrays {
	
	public static void main(String[] args) {
	        Number[] num = new Number[9];
	        
	        byte dataByte = 0b111;
	        num[0] = dataByte;
	        num[1] = new Byte(dataByte);   // byte — от -128 до 127
	        
	        short dataShort = 0x55ff;
	        num[2] = dataShort;   // short — от -32768 до 32767
	        num[3] = new Short((short) 0x55ff);   // short — от -32768 до 32767
	        
	        num[4] = new Integer(0x55aa0000);   // int — от -2147483648 до 2147483647
	        
	        num[5] = new Long(123_456);   // long — от -9223372036854775808 до 9223372036854775807
	        
	        float dataFloat = 123_456.789f;
	        num[6] = dataFloat;
	        num[7] = new Float(123_456F);   // float — от ~1,4*10-45 до ~3,4*1038
	        
	        num[8] = new Double(123_456.789D);   // double — от ~4,9*10-324  до ~1,8*10308
	        
	        for (Number item: num) {
		        System.out.println(item);
	        }
	    }
}

/*  out:
 * 
 * 7
 * 7
 * 22015
 * 22015
 * 1437204480
 * 123456
 * 123456.79
 * 123456.0
 * 123456.789
 */
