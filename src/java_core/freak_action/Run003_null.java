package java_core.freak_action;

public class Run003_null {
	public static void main(String args[]) {
	        Run003_null obj= null;
	        obj.staticMethod();
	        obj.nonStaticMethod();                             
	    }

	    private static void staticMethod() {
	        // Может вызываться по нулевой ссылке
	        System.out.println("static method, can be called by null reference");
	    }
	          
	    private void nonStaticMethod() {
	        // Невозможно вызвать по нулевой ссылке
	        System.out.print(" Non-static method- ");
	        System.out.println("cannot be called by null reference");
	    }
}

/*  out:
 *  static method, can be called by null reference
 *  Exception in thread "main" java.lang.NullPointerException
 *  	at test.Run003_null.main(Run003_null.java:8)
 */
