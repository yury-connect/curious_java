package java_core.freak_action;


/*
 * Деление целого на ноль недопустимо и выбрасывает ArithmeticException, 
 * но деление значения с плавающей запятой на ноль не вызывает исключения. 
 * Арифметика с плавающей запятой может переполняться до бесконечности, 
 * если результат операции слишком велик для double или float или стремиться к нулю, 
 * если результат слишком мал для double или float. 
 * Java предоставляет специальные значения с плавающей запятой 
 * POSITIVE_INFINITY, NEGATIVE_INFINITY и NaN (Not a Number – не число) для обозначения этих результатов. 
 * Эти значения определяются как специальные константы в классе Float и Double.
 * 
 * Если положительное число с плавающей запятой делится на ноль, 
 *  результат будет POSITIVE_INFINITY. 
 * Если отрицательное число с плавающей запятой делится на ноль, 
 *  результатом является NEGATIVE_INFINITY. 
 * Если ноль с плавающей точкой делится на ноль, 
 *  результатом является NaN, что означает, что результат математически не определен.
 * Если одним из операндов является NaN, то результатом будет NaN.
 */
public class Run008_Infinity_NaN {

	public static void main(String[] args) {
		System.out.println(1.0 / 0);   // Печатает Infinity
		System.out.println(-1*1.0 / 0);   // Печатает -Infinity
		System.out.println(0.0 / 0);   // Печатает NaN
		System.out.println(0.0 / +1);   // Печатает 0.0
		System.out.println(0.0 / -1);   // Печатает -0.0
	}
}

/*  out:
 *  
 *  Infinity
 *  -Infinity
 *  NaN
 *  0.0
 * -0.0
 */



/*	https://java9.ru/?p=227
 x		y		x/y		x*y		x%y		x+y		x-y
Конечное число	± 0.0		± infinity	± 0.0		NaN		Конечное число	Конечное число
Конечное число	± infinity	± 0.0		± 0.0		x		± infinity	infinity
± 0.0		± 0.0		NaN		± 0.0		NaN		± 0.0		± 0.0
± infinity	Конечное число	± infinity	± 0.0		NaN		± infinity	± infinity
± infinity	± infinity	NaN		± 0.0		NaN		± infinity	infinity
± 0.0		± infinity	± 0.0		NaN		± 0.0		± infinity	± 0.0
NaN		Любое число	NaN		NaN		NaN		NaN		NaN
Любое число	NaN		NaN		NaN		NaN		NaN		NaN
 */
