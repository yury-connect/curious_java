package java_core.unique_solution.who_called_me;


class TestingOneClass {
	public TestingOneClass() {
		action1();
	}
	private void action1() {
		action2();
	}
	private void action2() {
		action3();
	}
	private void action3() {
		new TestingTwoClass();
	}
}


class TestingThreeClass {
    public TestingThreeClass() {
        action3();
    }
    private void action3() {
        new WhoCalledMe();
    }
}


class TestingTwoClass {
    public TestingTwoClass() {
        action21();
    }
    private void action21() {
        action22();
    }
    private void action22() {
        new TestingThreeClass();
    }
}