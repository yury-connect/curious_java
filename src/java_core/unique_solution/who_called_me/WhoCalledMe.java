package java_core.unique_solution.who_called_me;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Метод определить 'КТО МЕНЯ ВЫЗВАЛ' через выброс исключения.
 */
public class WhoCalledMe {
	
	public WhoCalledMe() {
		List<StackTraceItem> calls = new ArrayList<>();
		try {
			throw new RuntimeException();
		}
		catch (RuntimeException e) {
			StackTraceElement[] stackTrace = e.getStackTrace();
			
			for(StackTraceElement oneCall: stackTrace) {   // переберем весть стек по цепочке;
				calls.add(new StackTraceItem(oneCall.getClassName(), oneCall.getMethodName()));
			}
		} finally {
			System.out.println(Arrays.toString(calls.toArray()));
		};
	}
	
	
	private class StackTraceItem {
		private final String className, metodName;
		public StackTraceItem(String className, String metodName) {
			this.className = className; this.metodName = metodName;
		}
		@Override
		public String toString() {
			return "\t [className = " + className + ", \t metodName = " + metodName + "]\n";
		}
	}

}
